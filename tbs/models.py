from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    UnicodeText,
    DateTime,
    String
    )

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )

from zope.sqlalchemy import ZopeTransactionExtension

import datetime

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()


class BusStop(Base):
    __tablename__ = 'busstop'
    id = Column(Integer, primary_key=True)
    name = Column(String(convert_unicode=True))
    number = Column(Integer)
    bdata = Column(Text,default = '')
    data = Column(Text, default = '')

    @classmethod
    def add_bdata(cls, name, number, new_bdata):
        row_count = DBSession.query(BusStop).filter(BusStop.number == number).count();
        if row_count > 1:
            raise Exception('Why more than row per bus number ?\n' )
            return
        
        row = DBSession.query(BusStop).filter(BusStop.number == number).first()
        
        if not row: #No rows present
            b = BusStop()
            b.name = name
            b.number = number
            b.bdata = new_bdata
            DBSession.add(b)
        else:
            row.bdata += ','+new_bdata
        
    def format_data(self):
        self.bdata = "{"+self.bdata+"}"
    
class Bus(Base):
    __tablename__ = 'bus'
    id = Column(Integer, primary_key=True,autoincrement=True)
    number = Column(Text)
    direction = Column(Integer)  #For each direction for a single bus , there will be 2 entries
    startstop = Column(Text)
    sid = Column(Integer)
    endstop = Column(Text)
    eid = Column(Integer)
    
    def __init__(self,number,direction,start,start_id,end,end_id):
        self.number = number
        self.direction = direction
        self.startstop = start
        self.sid = start_id
        self.endstop = end
        self.eid = end_id