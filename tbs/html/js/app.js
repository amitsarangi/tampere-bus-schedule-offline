
var stampUrl = '/api/stamp/'
var dataUrl = '/api/'
var partialsPath = '/home/js/partials/'

var tbs =angular.module('tbs', ['ngRoute'])
tbs.config(['$routeProvider','$locationProvider', function ($routeProvider,$locationProvider) {

    $routeProvider
    .when('/', {
        controller: 'IndexController',
        templateUrl: partialsPath+'index-view.html',
        controllerAs: "ctrl"
    })
    .when('/:id', {
        controller: 'ResultController',
        templateUrl: partialsPath+'result-view.html'
    })
    .otherwise({ redirectTo: '/' });

}]);

tbs.factory('dataSync',['$http', '$interval' ,function($http, $interval){

	var service = {};

	service.sync = function()
	{
		console.log("Checking Timestamp");
		//Get stamp data from API
		$http.get(stampUrl)
		.then(function(response) {
			if(window.localStorage.getItem('stamp') != response.data.stamp)
			{
				
				console.log("New Data found.Syncing");
				renewData(response.data.stamp);
			}
			else
			{
				console.log("Play On ! You already have the newest data.")
			}
		},function(response) {
			console.log("Unable to contact server");
		});
	}

	var renewData = function(s)
	{	
		$http.get(dataUrl)
		.then(function(response) {
			window.localStorage.setItem("busdata",JSON.stringify(response.data));
			window.localStorage.setItem("stamp",s);
			console.log("Sync Complete");
		}, function(response) {
			console.log("Some Error while syncing data");
		});

	}

	return service;
 }]);

tbs.directive('busstop', function() {
  return {
    restrict: 'A',
    scope: {
      data: '=busstop',
      history: '@history'
    },
    templateUrl: '/home/js/partials/stopData.html'
  };
});

tbs.controller('IndexController', ['$scope','dataSync','$location',function($scope, dataSync,$location){
	
	//Remove the results class from html
	document.getElementsByTagName('html')[0].classList.remove('results')
	
	$scope.history = window.localStorage.history ? JSON.parse(window.localStorage.getItem('history')) : [];

	$scope.stopdata = JSON.parse(window.localStorage.busdata)["stops"];

	$scope.go = function ( path ) {
	  $location.path( path );
	};

	$scope.removeFromHistory = function(num){
		//debugger;
		console.log(num)
		var history = JSON.parse(window.localStorage.getItem("history"))
		if(history.indexOf(num+'') != -1 ) console.log(history.splice(history.indexOf(num+''),1));
		window.localStorage.setItem("history",JSON.stringify(history))
		$scope.history = history;
		console.log("removed")
	}
	//Try syncing the data
	dataSync.sync();
}]);

tbs.controller('ResultController', ['$scope', '$routeParams', '$interval', function($scope, $routeParams, $interval){
	
	//add the results class to html
	document.getElementsByTagName('html')[0].classList.add('results')

	$scope.stopId = $routeParams.id;
	$scope.data = JSON.parse(window.localStorage.busdata)["data"][$scope.stopId];
	$scope.timetable = JSON.parse($scope.data.times)
	$scope.days = ["Monday-Friday","Saturday","Sunday"]	
	//Current viewing bus number
	$scope.current = -1;
	
	//Current time of the day
	$scope.day = new Date().getDay()
	$scope.hour = new Date().getHours()
	$scope.minute = new Date().getMinutes()


	$interval(function(){
		$scope.day = new Date().getDay()
		$scope.hour = new Date().getHours()
		$scope.minute = new Date().getMinutes()
	},60000);

	$scope.isActiveHour = function(h)
	{

		return (parseInt(h) == parseInt($scope.hour))  ? true : false;

	}

	$scope.isActiveWeek = function(w)
	{

		var evalDay = -1;
		if($scope.day >= 1 && $scope.dat <= 5)
		{
			evalDay = 0;
		}
		else if($scope.day == 6)
		{
			evalDay = 1;
		}
		else if($scope.day == 0)
		{
			evalDay = 2;
		}
		return (parseInt(w) == evalDay)  ? true : false;

	}

	$scope.isActiveMinute = function(m,prev,next)
	{
		return false;
	}

	$scope.formatMinute = function(m)
	{
		if($scope.current > 0 )
		{
			return m.replace("/"+$scope.current,"")
		}
		else
		{
			return m
		}
	}

	$scope.activate = function(number)
	{
		if($scope.data.bdata.length > 1)
		{
			$scope.current = number;
		}
	}

	$scope.unique = function(arr)
	{
		uniqueArray = arr.filter(function(item, pos) {
		    return arr.indexOf(item) == pos;
		});  //Checking if the index of the item is current index or not
		return uniqueArray;
	}

	//Adding to the history
	if(!window.localStorage.history)
	{
		var history = [];
		history.push($scope.stopId)
		window.localStorage.setItem("history",JSON.stringify(history))
	}
	else
	{
		var history = JSON.parse(window.localStorage.getItem("history"))
		if(history.indexOf($scope.stopId) == -1 ) history.push($scope.stopId)
		window.localStorage.setItem("history",JSON.stringify(history))
	}

}])

tbs.filter('searchFilter', function() {
  return function(stop_data, inputText) {
  	var filtered = []
    angular.forEach(stop_data, function(item) {

      if(item.name.toLowerCase().startsWith(inputText.toLowerCase()) || (''+item.number).indexOf(inputText) != -1) {
        filtered.push(item);
      }
    });

    //Sort the filtered results
    filtered.sort(function(a, b) {
    	return parseInt(a.number) - parseInt(b.number);
	});
    return filtered;
  };
});

tbs.filter('historyFilter', function() {
  return function(stop_data, history) {
  	var filtered = [];
    angular.forEach(stop_data, function(item) {
    	for(index in history)
    	{
    		if(item.number === parseInt(history[index]))
    		{
    			filtered.push(item);
    		}
    	}
    });

    return filtered.reverse();
  };
});

tbs.filter('debug', function() {
  return function(input) {
    if (input === '') return 'empty string';
    return input ? input : ('' + input);
  };
});

tbs.filter('unique', function () {

  return function (items, filterOn) {

		uniqueArray = items.filter(function(item, pos) {
		    return items.indexOf(item) == pos;
		});  //Checking if the index of the item is current index or not
		return uniqueArray;
  };
});