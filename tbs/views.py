from pyramid.response import Response
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPNotFound, HTTPFound

from sqlalchemy.exc import DBAPIError

from .models import (
    DBSession,
    BusStop,
    Bus
    )

import os,json
    
@view_config(route_name='home', renderer='json')
def my_view(request):
    return HTTPFound(location='/home/')

@view_config(route_name='api', renderer='json')
def api(request):
    #adding stop data
    stops = []
    bus_data = {}
    for s in DBSession.query(BusStop):
        stop = {}
        data = {}

        stop["name"] = s.name
        stop["number"] = s.number
        stop["towards"] = []

        data["number"] = s.number
        data["name"] = s.name
        data["towards"] = []
        data["times"] = s.data
        data["bdata"] = []

        bdata = json.loads(s.bdata)
        for n in bdata:
            data["bdata"].append(n)
            route = DBSession.query(Bus).filter(Bus.number == n, Bus.direction == bdata[n]).first()
            if route:
                stop["towards"].append(route.endstop)
                data["towards"].append(route.endstop)

        stops.append(stop)
        bus_data[s.number] = data

    #adding each stop data
    return { 'stops': stops ,'data': bus_data}

    
@view_config(route_name='stamp', renderer='json')
def stamp(request):
    dbpath = '../data/tbs.sqlite'
    return {'stamp':str(os.stat(os.path.join(os.path.dirname(__file__), dbpath)).st_size) }