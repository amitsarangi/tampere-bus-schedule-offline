import sys,os,urlparse,transaction,json,shutil
from pyramid.config import Configurator
from sqlalchemy import engine_from_config
from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

sys.path.append(os.path.abspath(os.path.join('..', '..', '..', 'tbs')))

from models import (
    DBSession,
    Base,
    Bus,
    BusStop
    )

from pyramid_mailer import Mailer
from pyramid_mailer.message import Message

config_uri = '../../../cron.ini'
base_url = 'http://aikataulut.tampere.fi/'
route_url_base = 'http://aikataulut.tampere.fi/?line='

settings = get_appsettings(os.path.join(os.path.dirname(__file__), config_uri))
engine = engine_from_config(settings, 'sqlalchemy.')
DBSession.configure(bind=engine)
Base.metadata.bind = engine
Base.metadata.drop_all(engine) #Drop all the tables
Base.metadata.create_all(engine)

#Configure the Mailer
mailer = Mailer.from_settings(settings);

from lxml import html
import requests

def get_name(el):
    stop_name = ''
    try:
        stop_name = el.text.strip()
    except AttributeError:
        stop_name = el.cssselect('span')[0].text.strip()
    return stop_name
    

base_page = requests.get(base_url)
base_html = html.fromstring(base_page.text)
bus_number_selector = '.lineSymbol td'
bus_numbers = [el.text for el in base_html.cssselect(bus_number_selector)]

for bus_no in bus_numbers:
    route_url = route_url_base+bus_no
    route_page = requests.get(route_url)
    route_html = html.fromstring(route_page.text)

    #Gather Bus line data
    
    lines = route_html.cssselect('table.lineDirection')
    for l in lines:
        start = get_name(l.cssselect('.stopName a')[0])
        start_id = l.cssselect('.stopName a span')[0].text[1:-1]
        end = get_name(l.cssselect('.stopName a')[-1])
        end_id = l.cssselect('.stopName a span')[-1].text[1:-1]
        direction = urlparse.parse_qs(urlparse.urlparse(l.cssselect('.stopName a')[0].get('href')).query)['direction'][0]
        
        b = Bus(bus_no,direction,start,start_id, end, end_id)
        DBSession.add(b)

    print 'Bus Line Added for Bus # ' + bus_no +'\n'
    #/Gather Bus line Data


    stop_selector = 'td.stopName a'
    stop_elements = route_html.cssselect(stop_selector)
    #Gather Stop data
    for el in stop_elements:
        
        stop_name = get_name(el)
        print 'Start Adding stops for Bus # ' + bus_no+'/'+ stop_name + "\n"
        stop_number = urlparse.parse_qs(urlparse.urlparse(el.get('href')).query)['stop'][0]
        bus_direction = urlparse.parse_qs(urlparse.urlparse(el.get('href')).query)['direction'][0]
        BusStop.add_bdata(stop_name,stop_number,'"'+bus_no+'"'+":"+'"'+bus_direction+'"')
        print 'Finished Adding stops for Bus # ' + bus_no+'/'+ stop_name + "\n"
    #/Gather stop data
    
#gather stop details
for row in DBSession.query(BusStop):
    #format the data
    row.format_data()
    print 'Getting Data for Stop  : ' + str(row.number)
    stop_url = base_url+"?stop=" +str(row.number)
    stop_page = requests.get(stop_url)
    stop_html = html.fromstring(stop_page.text)
    data = {}
    for i,el in enumerate(stop_html.cssselect('.timetable ')):
        values = {}
        for e in el.cssselect('tr'):
            values[e.cssselect('.timetableHour')[0].text.strip()] = [t.text_content().strip() for t in e.cssselect('.items .timetableItem')]
        data[i] = values
    row.data = json.dumps(data)

print 'Committing to Database\n'
transaction.commit()
print 'Committed to Database'

#Compare the files and change if necessary
#Only the file size for now for testing
noChange = False
temp_file = "../../../data/tbs_temp.sqlite"
orig_file = "../../../data/tbs.sqlite"
if str(os.stat(temp_file).st_size) == str(os.stat(orig_file).st_size) :
    noChange = True
else:
    #Replace the file
    shutil.copy(temp_file,orig_file)


mailer.send_immediately(Message(subject="Cron Status : Done",
              sender='mailer@amitsarangi.me',
              recipients=["amit.getinfo@gmail.com"],
              body="Change "+ ("Not " if noChange else "")+"Detected"))