var tbs = angular.module('tbs',[])

tbs.factory('dbcache', function($q) {
  var factory;
  return factory = {
    getItem: function(table, key) {
      var db, deferred;
      deferred = $q.defer();
      db = window.sqlitePlugin.openDatabase({
        name: "app_dbcache"
      });
      db.transaction(function(tx) {
        return tx.executeSql("select data from " + table + " where key='" + key + "';", [], function(tx, res) {
          if (res.rows.length > 0) {
            return deferred.resolve(res.rows.item(0).data);
          } else {
            return deferred.resolve(false);
          }
        });
      });
      return deferred.promise;
    }
  };
});


tbs.controller('SearchControl', function ($scope) {
  
});

tbs.config(function($routeProvider){
	$routeProvider
	.when('/',{
		templateUrl : ""
		controller : "SearchControl"
	})
	.otherwise({ redirectTo: '/' });

})